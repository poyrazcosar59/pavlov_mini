#!/usr/bin/env python
import rospy
import rospkg

rospack = rospkg.RosPack()


import pybullet as pb
import time
import pybullet_data
import numpy as np
from pavlov_mini_sim import pavlov_mini_sim

from sensor_msgs.msg import JointState, Imu
from pavlov_mini_msgs.msg import contact_detection

class PavlovMiniPybullet:
    def __init__(self, urdf_dir, robot_name):
        self.debug("INFO", "pavlov_mini_pybullet_node is running!")



        ### Pybullet parameters ###
        pb.setTimeStep(0.001)
        pb.setGravity(0, 0, -9.8)
        ground = pb.loadURDF("plane.urdf")
        pb.changeDynamics(ground, -1, lateralFriction=1, spinningFriction=0.01, rollingFriction=0.01)


        #### pavlov mini ####
        startpos = [0, 0, 0.26]  # meters
        startOri = [0, 0, 0]     # degrees
        self.pavlov_mini = pavlov_mini_sim(urdf_dir, startpos, startOri, ground)
        self.jointStateMSg = JointState()
        self.jointStateMSg.name = list(self.pavlov_mini.getRevoluteJointNames())
        self.feet_contact = np.zeros(4, dtype = 'bool')
        self.contact_det_msg = contact_detection()

        ####### publisher ##########
        self.jointStatePub = rospy.Publisher('/' + robot_name + '/joints_state', JointState, queue_size=10)
        self.robotImuPub = rospy.Publisher('/' + robot_name + '/imu_data',  Imu, queue_size=10)
        self.contactDetectionPub = rospy.Publisher('/' + robot_name + '/contact_detection',  contact_detection, queue_size=10)





        ####### subscribers ########
        rospy.Subscriber('/' + robot_name + '/joints_control', JointState, self.jointControlCB)




    """ call backs functions """
    def jointControlCB(self, data):
        self.pavlov_mini.setJointPosition(data.position, velocity = 1)


    """ Code for the main thread of the node """
    def mainThread(self):

        basePos, baseOrn = pb.getBasePositionAndOrientation(1)  # Get model position
        pb.resetDebugVisualizerCamera(cameraDistance=1, cameraYaw=45, cameraPitch=-20,
                                     cameraTargetPosition=basePos)  # fix camera onto model

        pb.stepSimulation()

        self.feet_contact = self.pavlov_mini.getFeetContact()

        #self.publishJointState()
        self.publishIMUdata()
        self.publishContactDetectorData()
    def publishJointState(self):
        pos, vel, torq = self.pavlov_mini.getMotorJointStates()
        self.jointStateMSg.position = pos
        self.jointStateMSg.velocity = vel
        self.jointStateMSg.effort   = torq
        self.jointStateMSg.header.stamp = rospy.Time.now()
        self.jointStatePub.publish(self.jointStateMSg)

    def publishContactDetectorData(self):
        self.contact_det_msg.header.stamp = rospy.Time.now()
        self.contact_det_msg.feet_stance = [self.feet_contact[0],self.feet_contact[1],self.feet_contact[2],self.feet_contact[3]]
        #self.contact_det_msg.feet_stance.append(self.feet_contact[0])
        self.contactDetectionPub.publish(self.contact_det_msg)

    def publishIMUdata(self):
        pos, vel, ang_pos, ang_vel, acc, ang_acc = self.pavlov_mini.getRobotState()

        imu_msg = Imu()
        imu_msg.header.stamp = rospy.Time.now()
        imu_msg.header.frame_id = "base_link"

        imu_msg.orientation.x, imu_msg.orientation.y, imu_msg.orientation.z, imu_msg.orientation.w = ang_pos

        imu_msg.angular_velocity.x, imu_msg.angular_velocity.y, imu_msg.angular_velocity.z = ang_vel
        imu_msg.linear_acceleration.x, imu_msg.linear_acceleration.y, imu_msg.linear_acceleration.z = acc


        self.robotImuPub.publish(imu_msg)





    def debug(self, typ, msg):
        print(typ + ": " + msg + "\n")


if __name__ == '__main__':
    try:
        rospy.init_node('pavlov_mini_pybullet_node', anonymous=True)
        rate = rospy.Rate(1000)    # 10 Hz

        robot_name = "pavlov_mini"
        urdf_dir  = rospack.get_path('pavlov_mini_description') + '/urdf/pavlov_mini.urdf'
        if rospy.has_param('/robot_name'):
            robot_name = rospy.get_param("/robot_name")

        if rospy.has_param('/urdf_path'):
            urdf_dir = rospy.get_param("/urdf_path")

        client = pb.connect(pb.GUI)  # or p.DIRECT for non-graphical version
        pb.setAdditionalSearchPath(pybullet_data.getDataPath())  # optionally

        node = PavlovMiniPybullet(urdf_dir, robot_name)

        while not rospy.is_shutdown():
            node.mainThread()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
    pb.disconnect()

