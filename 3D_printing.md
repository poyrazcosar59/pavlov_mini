# 3D printing
All the components of the robot, except for the electronics and bearings, are 3D printed. This this how the assembly of the robot looks like:

![Demo](images/3d_printed_parts.gif)


If you want to take a look to all the parts and how they should be assembled together, you can explore this [Blender file](3D_printing/pavlov_mini_assembly.blend). If you don't have Blender, you can download it for free from [the Blender website](https://www.blender.org/)



Before printing all the parts, you need to make sure that the bearings fit well in all the parts. Because this highly depends on the calibration parameters of the 3D printer, I first recommend to print as a test one [part of the femur](3D_printing/femurs/femur_r_b.stl), and see how the big and small bearings fit. If everything is fine, you can print the rest of the parts. However, if you think the bearings are too loose, you will need to modify the mesh files of all the [hips](3D_printing/hips/) and the [femurs](3D_printing/femurs/) before printing. On the other hand, if the hole for the bearings is too small, you can always use sand paper to enlarge the hole.

This table shows the parts you need to print and the quantity of each. I recommend to print each part in the given orientation.

## Body parts
| Body parts | Quantity | 
| --- | --- |
| [Body Front-Left](3D_printing/body/body_fl.stl) | 1 |
| [Body Front-Right](3D_printing/body/body_fr.stl) | 1 |
| [Body Back-Left](3D_printing/body/body_bl.stl) | 1 |
| [Body Back-Right](3D_printing/body/body_br.stl) | 1 |



## Hip parts
| Hip parts | Quantity | 
| --- | --- |
| [Hip Front-Left](3D_printing/hips/hip_fl.stl) | 1 |
| [Hip Front-Right](3D_printing/hips/hip_fr.stl) | 1 |
| [Hip Back-Left](3D_printing/hips/hip_bl.stl) | 1 |
| [Hip Back-Right](3D_printing/hips/hip_br.stl) | 1 |

## Femur parts
| Femur parts | Quantity | 
| --- | --- |
| [Femur A Left](3D_printing/femurs/femur_l_a.stl) | 2 |
| [Femur B Left](3D_printing/femurs/femur_l_b.stl) | 2 |
| [Femur A Right](3D_printing/femurs/femur_r_a.stl) | 2 |
| [Femur B Right](3D_printing/femurs/femur_r_b.stl) | 2 |


## Tibia parts
| Tibia parts | Quantity | 
| --- | --- |
| [Tibia Left](3D_printing/tibias/tibia_l.stl) | 2 |
| [Tibia Right](3D_printing/tibias/tibia_r.stl) | 2 |

## Feet parts
| Tibia parts | Quantity | 
| --- | --- |
| [Foot Left](3D_printing/tibias/foot_l.stl) | 2 |
| [Foot Right](3D_printing/tibias/foot_r.stl) | 2 |


## Supports
### Batery
| Support parts | Quantity | 
| --- | --- |
| [Battery support Front](3D_printing/body_supports/battery_support_front.stl) | 1 |
| [Battery support Back](3D_printing/body_supports/battery_support_back.stl)   | 1 |

### Raspberry pi
| Support parts | Quantity | 
| --- | --- |
| [Raspi support Left](3D_printing/body_supports/raspi_support_l.stl) | 1 |
| [Raspi support Right](3D_printing/body_supports/raspi_support_r.stl)   | 1 |

### Bottom support
| Support parts | Quantity | 
| --- | --- |
| [Bottom support Left](3D_printing/body_supports/bottom_support_l.stl) | 1 |
| [Bottom support Right](3D_printing/body_supports/bottom_support_r.stl)   | 1 |
| [Back cover connector](3D_printing/body/back_cover_connector_from_below.stl) | 1 |
| [Front cover connector](3D_printing/body/front_cover_connector_from_below.stl) | 1 |

### IMU support
| Support parts | Quantity | 
| --- | --- |
| [IMU support](3D_printing/body_supports/IMU_support.stl) | 1 |


## Covers
| Cover parts | Quantity | 
| --- | --- |
| [Face](3D_printing/body/face.stl) | 1 |
| [Front frame](3D_printing/body/front_cover_frame.stl) | 1 |
| [Back cover](3D_printing/body/back_cover.stl) | 1 |
| [Top cover half back](3D_printing/body/top_cover_half_back.stl) | 1 |
| [Top cover half front](3D_printing/body/top_cover_half_forward.stl) | 1 |


# Assembly

For details to the assembly of all components, please check my youtube video:

[![IMAGE ALT TEXT](https://i.ytimg.com/vi/SU3fmMUz9Zg/hqdefault.jpg?sqp=-oaymwEbCKgBEF5IVfKriqkDDggBFQAAiEIYAXABwAEG\u0026rs=AOn4CLCb7KFHu5iitzOF58ZOoL_JJV5BwQ)](http://www.youtube.com/watch?v=SU3fmMUz9Zg&t=206s&ab=Androtics "How to Design a DIY Quadruped Robot
")

