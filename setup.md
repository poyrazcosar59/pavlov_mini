# Setting up the environment
To run the code you don't need a physical robot or a raspberry pi. You can test it with just a computer running ROS. Follow the next:

## Ros melodic (deprecated)
The initial version of the robot works in ROS melodic, for example using Ubuntu 18.04 LTS (although the version 19.04LTS should work as well, but not Ubuntu 20.04LTS). This version is not continued for development!, so please refer to the next section for the new update on ROS noetic. To run the software in ROS melodic, you need to follow the next steps:

The first step is to install ROS. Follow the instructions in the following link:
http://wiki.ros.org/melodic/Installation/Ubuntu

Next, create a workspace with the following tutorial:
http://wiki.ros.org/catkin/Tutorials/create_a_workspace

Clone the Pavlov mini project in the /src/ folder of your workspace:
> git clone https://gitlab.com/anflores/pavlov_mini/

Change the repository to the ROS melodic branch:

> git checkout ros_melodic

Now build the project:
> cd ..

> catkin_make

Now refer to the section "Test the code"



## Ros Noetic (New)
This is the newest version of the software and all the development is happening here. You can use Ubuntu 18.04LTS or newer versions (It has been tested with Ubuntu 20.04LTS).

First, download and install Anaconda3:
https://www.anaconda.com/


Next, clone the repository in your home directory
> git clone https://gitlab.com/anflores/pavlov_mini/

Create the ros_tf environment from the [yml](ros_tf.yml) file:

> conda env create -f ros_tf.yml

This environment contains ROS Neotic plus tensorflow for future deep learning integration.

Activate the conda environment:

> conda activate ros_tf

If you want to have the ros_tf environment by default activated every time you open a terminal, modify the ".bashrc" file from your home directory:

> nano ~/.bashrc

now go to the end and write in a last new line:

> conda activate ros_tf

and save the ".bashrc" file pressing Ctrl + x

Next, create a workspace with the following tutorial:
http://wiki.ros.org/catkin/Tutorials/create_a_workspace


Move the Pavlov mini folder to the /src/ folder of your workspace:
> mv ~/pavlov_mini/ YOUR-CATIN-WORKSPACE/src/pavlov_mini

And finally build the project:
> cd ~/YOUR-CATIN-WORKSPACE/

> catkin_make




# Test the code
In principle you don't need to build your own 3D printed robot to run the code. You can test and simulate the robot in your computer.
First launch the test file in a terminal:
> roslaunch gait_control gait_control_test.launch

If everything works, 2 windows should open. The first one is rviz, that should show the robot after a few seconds. The second window is a black window that to control the robot. Right now the robot is in 'stop' mode. To make it walk, run in a different terminal the following command:

> rosservice call /pavlov_mini/set_gait "gait_filename: 'diagonal_fast'" 

After this, you should see the robot dog moving the diagonal legs. You can set different gaits for the robot, which are defined in files within the gait_control package (pavlov_mini/gait_control/gaits/). Remember not to include the '.gait' extension file when calling the rosservice "/pavlov_mini/set_gait".

Now you can use the black window to control the robot using the keys "w", "s", "d", "a", "q", "e".



## Use the code in a physical robot
To use the code in a real robot, you need a Teensy 4.0 board with a IMU BNO 080 sensor. 
First, you need to copy the content of the folder /pavlov_mini/arduino/libraries/ into your Arduino libraries (usually when you install Arduino IDE in Ubuntu, your libraries are located in in ~/Arduino/libraries/).

Then you need install Teensyduino (an add-on to upload arduino scripts to Teensy boards). Follow this link:
https://www.pjrc.com/teensy/teensyduino.html

Finally, you need to upload the Arduino script located in the folder /pavlov_mini/arduino/pavlov_mini_arduino/ into your Teensy board. 

If everything is good, you should be able to run the following command in a terminal:

> roslaunch gait_control gait_control_with_visualization.launch

If you receive an error about 'rosserial not recognized', the install rosserial with the following command

> sudo apt install ros-noetic-rosserial

If you receive an error about 'could not open port /dev/ttyACM0', then you need to change the serial port where your Teensy is connected. You need to open the file 
/pavlov_mini/pavlov_mini_calibration/launch/connect_teensy.launch 
and edit the 4th line and in 'value', you need to specify the port where Teensy is connected. You can look up this port with the arduino IDE.





