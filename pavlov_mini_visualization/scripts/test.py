"""
# Sample Python/Pygame Programs
# Simpson College Computer Science
# http://programarcadegames.com/
# http://simpson.edu/computer-science/
"""

import pygame
import time
# Definimos algunos colores
NEGRO = (0, 0, 0)
BLANCO = (255, 255, 255)


pygame.init()
pygame.joystick.init()

# -------- Bucle Principal del Programa -----------
while True:
    # PROCESAMIENTO DEL EVENTO
    for evento in pygame.event.get():
        if evento.type == pygame.QUIT:
            hecho = True

        # Acciones posibles del joystick: JOYAXISMOTION JOYBALLMOTION JOYBUTTONDOWN JOYBUTTONUP JOYHATMOTION
        if evento.type == pygame.JOYBUTTONDOWN:
            print('Boton presionado del joystick.')
        if evento.type == pygame.JOYBUTTONUP:
            print("Boton liberado del joystick.")


    joystick_count = pygame.joystick.get_count()

    print("Numero de joysticks: {}".format(joystick_count))

    # Para cada joystick:
    for i in range(joystick_count):
        joystick = pygame.joystick.Joystick(i)
        joystick.init()

        print("Joystick {}".format(i))

        # Obtiene el nombre del Sistema Operativo del controlador/joystick
        nombre = joystick.get_name()
        print("Nombre del joystick: {}".format(nombre))

        # Habitualmente, los ejes van en pareja, arriba/abajo para uno, e izquierda/derecha
        # para el otro.
        ejes = joystick.get_numaxes()
        print("Numero de ejes: {}".format(ejes))

        for i in range(ejes):
            eje = joystick.get_axis(i)
            print("Eje {} valor: {:>6.3f}".format(i, eje))

        botones = joystick.get_numbuttons()
        print("Numero de botones: {}".format(botones))

        for i in range(botones):
            boton = joystick.get_button(i)
            print("Boton {:>2} valor: {}".format(i, boton))

        # El valor vuelve en un array.
        hats = joystick.get_numhats()
        print("Numero de hats: {}".format(hats))

        for i in range(hats):
            hat = joystick.get_hat(i)
            print("Hat {} valor: {}".format(i, str(hat)))

    print("\n\n\n\n")
    time.sleep(0.1)

pygame.quit()